<<<<<<< HEAD
﻿// Array_Cal.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//
//用类/函数来实现:
//一、需求：希望返回 3 种信息: 最大子数组的和,最大子数组开始的下标,最大子数组结束的下标
//二、从文本文件中读输入的数据，熟悉文件操作， 文件有两种数据。第一个数字：这次测试中有多少个数据， 数字后面是冒号。后续数字： 每个数据的值，用逗号隔开
#include<iostream>
#include"Array_Cal.h"
#include<cstdlib>

#define N 1000
#define Max 5000

using namespace std;

//分治法
R Max_Sum(int A[], int low, int mid, int high)
{
	R b;
	int Max_L = mid;//左边最大值位置
	int Max_SL = -Max;//左边最大和
	int SL = 0;
	//左边
	for (int i = mid;i >= low;i--)
	{
		SL += A[i];
		if (SL > Max_SL)
		{
			Max_SL = SL;
			Max_L = i;
		}
	}
	//右边	
	int Max_R = mid + 1;//右边最大值位置
	int Max_SR = -Max;//右边最大和
	int SR = 0;
	for (int i = mid+1;i <= high;i++)
	{
		SR += A[i];
		if (SR > Max_SR)
		{
			Max_SR = SR;
			Max_R = i;
		}
	}
	b.l = Max_L;
	b.r = Max_R;
	b.s = Max_SR + Max_SL;
	return b;
}


int main()
{
	int a[] = {-1,1,2,6,5,4,3};
	R t=Max_subArray(a,0,6);
	cout << t.l<<"  "<<t.r<<"  "<<t.s;

}
//分治法实现查找最大数组
R Max_subArray(int A[], int start, int end)//A表示所分析的数组
{
	R b,t1,t2,t3;
	if (start == end)
	{
		b.l = start;
		b.r = end;
		b.s = A[start];
		return b;
	}
	else
	{
		int mid = (start + end) / 2;
		t1 = Max_subArray(A, start, mid);
		t2 = Max_subArray(A, mid + 1, end);
		t3 = Max_Sum(A, start, mid, end);
	}
	if (t1.s < t2.s || t1.s < t3.s)
	{
		if (t2.s < t3.s) return t3;
		else return t2;
	}
	else return t1;
}
//ifstream infile;   //输入流
//ofstream outfile;   //输出流

//infile.open("E:\\计算与软件工程\\程序\\C++\\Array_Caldata.txt", ios::in);
//if (!infile.is_open())
//	cout << "Open file failure" << endl;
//int k = 0,co;
//while (!infile.eof())            // 若未到文件结束一直循环
//{
//	infile>>co;
//	if (co != 44)
//	{
//		a[k++] = co;
//	}
=======
﻿// Array_Cal.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//
//用类/函数来实现:
//一、需求：希望返回 3 种信息: 最大子数组的和,最大子数组开始的下标,最大子数组结束的下标
//二、从文本文件中读输入的数据，熟悉文件操作， 文件有两种数据。第一个数字：这次测试中有多少个数据， 数字后面是冒号。后续数字： 每个数据的值，用逗号隔开
#include<iostream>
#include"Array_Cal.h"
#include<cstdlib>

#define N 1000
#define Max 5000

using namespace std;

//分治法
R Max_Sum(int A[], int low, int mid, int high)
{
	R b;
	int Max_L = mid;//左边最大值位置
	int Max_SL = -Max;//左边最大和
	int SL = 0;
	//左边
	for (int i = mid;i >= low;i--)
	{
		SL += A[i];
		if (SL > Max_SL)
		{
			Max_SL = SL;
			Max_L = i;
		}
	}
	//右边	
	int Max_R = mid + 1;//右边最大值位置
	int Max_SR = -Max;//右边最大和
	int SR = 0;
	for (int i = mid+1;i <= high;i++)
	{
		SR += A[i];
		if (SR > Max_SR)
		{
			Max_SR = SR;
			Max_R = i;
		}
	}
	b.l = Max_L;
	b.r = Max_R;
	b.s = Max_SR + Max_SL;
	return b;
}


int main()
{
	int a[] = {-1,1,2,6,5,4,3};
	R t=Max_subArray(a,0,6);
	cout << t.l<<"  "<<t.r<<"  "<<t.s;

}
//分治法实现查找最大数组
R Max_subArray(int A[], int start, int end)//A表示所分析的数组
{
	R b,t1,t2,t3;
	if (start == end)
	{
		b.l = start;
		b.r = end;
		b.s = A[start];
		return b;
	}
	else
	{
		int mid = (start + end) / 2;
		t1 = Max_subArray(A, start, mid);
		t2 = Max_subArray(A, mid + 1, end);
		t3 = Max_Sum(A, start, mid, end);
	}
	if (t1.s < t2.s || t1.s < t3.s)
	{
		if (t2.s < t3.s) return t3;
		else return t2;
	}
	else return t1;
}
//ifstream infile;   //输入流
//ofstream outfile;   //输出流

//infile.open("E:\\计算与软件工程\\程序\\C++\\Array_Caldata.txt", ios::in);
//if (!infile.is_open())
//	cout << "Open file failure" << endl;
//int k = 0,co;
//while (!infile.eof())            // 若未到文件结束一直循环
//{
//	infile>>co;
//	if (co != 44)
//	{
//		a[k++] = co;
//	}
>>>>>>> b65496804b4f7a4867e146f5d2e64dbd460cac86
//}