﻿#include <iostream>
#include <cstdlib>
#include <string>  
#include <vector>  
#include <fstream> 
#include <sstream> 
#include <assert.h>

using namespace std;

const int rangeNum = 10;
const int bufferSize = 98196;
char buffer[bufferSize];
//char print[rangeNum][100] = { "高俅","端王","王进","史进","陈达","杨春","李吉" };//第一回
char print[rangeNum][100] = {"晁盖","宋江","宋清","黄信","花荣","秦明","燕顺" ,"石勇","林冲","刘唐"};//第34回
//char print[rangeNum][100] = {"石秀","宋江","吴用","卢俊义","戴宗","朱仝","花荣","秦明","索超" ,"董澄"};//第91回

int ans[rangeNum];

int main()
{
	ifstream fin;
	fin.open("E:\\大三下\\计算与软件工程\\程序\\C++\\HLM\\水浒传.txt");   //将文件流对象与文件连接起来 
	memset(ans, 0, sizeof(ans));
	int ch, t = 0;
	char c, cc, ccc, cccc;
	cout << "请输入统计的章节号：";
	cin >> ch;
	//第  回
	bool flg;
	while (fin.getline(buffer, bufferSize))//查找章节
	{
		flg = false;
		int k;
		for (k = 0; k < 12; k++)
		{
			if (buffer[k] != '-') break;
		}
		if (k == 12)
		{
			t++;
			if (t == ch+2)
			{
				flg = true;
				for(int i=0;i<6;i++)
					fin.getline(buffer, bufferSize);
				break;
			}
		}
	}
	if (flg)
	{
		c = fin.get(); 
		cc = fin.get();
		ccc = fin.get();
		while (c != EOF && c != '-')
		{
			cccc = fin.get();
			for (int i = 0; i < rangeNum; i++)
			{
				if (c == print[i][0] && cc == print[i][1] && ccc == print[i][2] && cccc == print[i][3])
				{
					++ans[i];
				}
			}
			c = cc;
			cc = ccc;
			ccc = cccc;
		}
	}

	for (int i = 0; i < rangeNum; ++i)
	{
		cout << print[i] << ":" << ans[i] << endl;
	}
	fin.close();           //关闭文件输入流 
	// 写文件
	ofstream outFile;
	outFile.open("Role.csv", ios::out); // 打开模式可省略  
	//outFile << "第"<<ch<<"回"<< endl;
	outFile << "名称" << ',' << "第" << ch << "回出现次数" << endl;
	for (int i = 0; i < rangeNum; i++)
	{
		outFile << print[i] << ',' << ans[i] << endl;
	}
	outFile.close();
	getchar();
	system("pause");
	return 0;
}